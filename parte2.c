#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAX 100

//funcion para remplazar los saltos de linea de fgets por caracteres nulos
void remplazar(char linea[],char viejo,char nuevo){
	char nuevoArreglo[MAX]={0};
	strcpy(nuevoArreglo,linea);
	for(int i=0; i<sizeof(nuevoArreglo);i++){
		if(linea[i]==viejo){
			linea[i]=nuevo;
		}
	}
}


int main(){
	int valor;
	float promedio;
	int bandera= 1;
	int contador=0;
	int  max=0;
	int min;
	char linea[MAX]={0};
	int acumulador=0;	
	while(bandera == 1){
		
		fgets(linea,MAX,stdin); //variable, tamano arreglo, ingreso por teclado
		remplazar(linea,'\n','\0'); 
		valor=atoi(linea); //convierte a entero
		acumulador=acumulador+valor; //0+numero ingresado
		contador++; //incrementa
		if (valor > max){
			max= valor;
		}
		if (valor < min || contador == 1){
			min= valor;
			printf("%d",min);
		} 
		if(strcmp(linea,"x") == 0){ //compara si son iguales
			bandera=-1;	
			//acumulador=acumulador+1;			contador=contador-1; //
		}
	} 	
	promedio=((float)acumulador/contador);
	printf("%.2f\t%d\t%d",promedio,max,min);			
} 
